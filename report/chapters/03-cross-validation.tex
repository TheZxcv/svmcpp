\section{Cross-Validazione}\label{sec:val}

La \emph{cross-validazione} \`e una tecnica di stima dell'accuratezza di un classificatore.
Viene solitamente utilizzata per stimare l'accuratezza media dei classificatori prodotti
da un algoritmo di apprendimento oppure per guidare la scelta dei parametri migliori.

Generalmente, la tecnica della cross-validazione consiste nell'individuare
una parte del training set, chiamata \emph{validation set},
ed effettuare l'addestramento del classificatore sul training set sottratto degli elementi
presenti nel validation set.
Dopodich\'e, le prestazione vengono valutate calcolando l'accuratezza del classificatore
risultante sul validation set.

La tipologia di cross-validazione utilizzata per la stima dei parametri $\lambda$ e $T$ \`e \emph{k-fold}.

\subsection{K-Fold}\label{sec:kfold}

Per determinare i valori migliori dei parametri $\lambda$ e $T$ del classificatore
\`e stata effettuata una validazione interna tramite \emph{k-fold} del training set.

La tecnica k-fold consiste nel partizionare il training set in $k$ partizioni $D_1, \ldots, D_k$
tali che $S = \cup_{i=1, \ldots, k} D_i$.

\begin{figure}[h]
    \centering

    \resizebox{\linewidth}{!}{%
        \begin{tikzpicture}[node distance=0cm, outer sep=0pt]
            \tikzstyle{common}=[text centered, minimum height=2.5em]
            \tikzstyle{partition}=[common, draw, fill=gray!10, minimum width=5em]

            \node (S) [common, anchor=north west] {$S =$};
            \node (D1) [partition, anchor=north west] at (S.north east) {$D_1$};
            \node (D2) [partition, anchor=north west] at (D1.north east) {$D_2$};
            \node (dots) [partition, anchor=north west, minimum width=10em] at (D2.north east) {\ldots};
            \node (Dk) [partition, anchor=north west] at (dots.north east) {$D_k$};
        \end{tikzpicture}%
    }
\end{figure}

\medskip

Dunque, si addestrano $k$ classificatori sugli insiemi $S_i = S \smallsetminus D_i$ come mostrato
in fig.~\ref{fig:kfold}.

\begin{figure}[h]
    \centering

    \begin{tikzpicture}[node distance=0cm, outer sep=0pt]
        \tikzstyle{common}=[text centered, minimum height=1em]
        \tikzstyle{partition}=[common, draw, fill=gray!10, minimum width=2.5em]
        \foreach \i in {1, ..., 5}
            {
                \node (S\i)  [common, anchor=north west] at (0, 5 - \i) {$S_\i =$};

                \ifnum\i=1\relax\def\partitionColor{orange!10}\else\def\partitionColor{green!50}\fi
                \node (D1\i) [partition, anchor=north west, fill=\partitionColor] at (S\i.north east) {$D_1$};
                \foreach \j in {2, ..., 5}
                    {
                        \ifnum\i=\j\relax\def\partitionColor{orange!10}\else\def\partitionColor{green!50}\fi
                        \pgfmathparse{int(\j - 1)}
                        \node (D\j\i) [partition, anchor=north west, fill=\partitionColor] at (D\pgfmathresult\i.north east) {$D_\j$};
                    }
            }
    \end{tikzpicture}
    \caption{Esempio di 5-fold di un training set. Le partizioni in verde sono utilizzate per l'addestramento del classificatore.
        Le partizioni in arancione sono utilizzate per valutare l'accuratezza del classificatore prodotto.}
    \label{fig:kfold}
\end{figure}

Per ogni classificatore $h_i$, si calcola il test error sulla partizionare associata $D_i$.
La media di questi errori ci permette di stimare l'errore medio dei classificatori prodotti
dall'algoritmo di apprendimento scelto in modo cos\`i da poterne valutare la qualit\`a.

\begin{equation*}
    \frac{1}{k} \sum_{i=1}^{k} \left(\frac{k}{\| S \|} \sum_{(\vec{x}, y) \in S_i} \ell (y, h_i(\vec{x}))  \right)
\end{equation*}

\subsection{Exhaustive Grid Search}\label{sec:grid-search}

Utilizzando la tecnica k-fold vista in precedenza \`e possibile stimare le prestazioni
dell'algoritmo di apprendimento data una valorizzazione dei parametri.
Rimane per\`o il problema di capire quali valori dei parametri utilizzare.
Per risolvere questo problema \`e stato utilizzato l'algoritmo di ricerca
\emph{Exhaustive Grid Search}.

Esso consiste nel calcolare l'errore medio tramite cross-validazione k-fold
per tutte le coppie di valori dei parametri data una lista di valori per ogni parametro.
I valori scelti dall'algoritmo sono quelli che producono errore medio minore.

In fig.~\ref{fig:heatmap} possiamo osservare un esempio di griglia prodotta
dall'algoritmo. In questo caso, l'algoritmo sceglierebbe i valori
$\lambda = 0.0001$ e $T = 5000$ oppure $\lambda = 0.001$ e $T = 5000$
perch\'e entrambi hanno prodotto l'errore medio minimo.

\begin{figure}[h]
    \includegraphics[width=\linewidth]{{{figures/label33/heatmap}}}
    \caption{Mappa di calore errore per ogni coppia di $\lambda$ e $T$ provata.}
    \label{fig:heatmap}
\end{figure}
