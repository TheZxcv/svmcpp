\section{Support-vector Machine}\label{sec:algos}

Support-vector machine (SVM) \`e un algoritmo di apprendimento per classificatori lineari
introdotto nel 1995~\cite{cortes1995support}.
L'idea su cui si basa l'algoritmo \`e trovare l'iperpiano che separa
gli esempi positivi da quelli negativi con margine massimo.
Il margine \`e definito come la distanza minima tra l'iperpiano e gli esempi.

\begin{figure}[h]
    \centering
    \includegraphics[width=.75\linewidth]{{{figures/svm-with-supports}}}
    \caption{Esempio in 2 dimensioni di classificazione tramite SVM.
        In rosso sono stati riportati gli esempi positivi ed in blu quelli negativi.
        In verde \`e stato riportato il piano separatore con il rispettivo vettore $\vec{w}$ in nero.
        Cerchiati sono i supporti e con i rispettivi piani tratteggiati.}
    \label{fig:svm-with-supports}
\end{figure}

Per definire il problema di ottimizzazione del margine, consideriamo inizialmente
una versione semplificata sui vettori
$\vec{u} \in \Real^d$ tali che $\norm{\vec{u}} = 1$.
Siano $(\vec{x}^+, y^+)$ e $(\vec{x}^-, y^-)$
rispettivamente un esempio positivo ed un esempio negativo dal training set $S$.
Il margine tra $\vec{u}$ e $\vec{x}^+$ \`e dato da $\vec{u}^T \vec{x}^+$ ed
il margine tra $\vec{u}$ e $\vec{x}^-$ \`e dato da $-\vec{u}^T \vec{x}^-$.
Moltiplicando entrambi i margini per la corrispettiva etichetta, otteniamo
che il margine del generico $(\vec{x}_i, y_i) \in S$ \`e dato da:
\begin{equation}
    y_i \vec{u}^T \vec{x}_i
\end{equation}

Dunque il problema di massimizzazione del margine minimo si traduce nel problema:
\begin{equation}
    \vec{u}^* = \argmax_{\vec{u} : \norm{\vec{u}} = 1}
    \left(\min_{(\vec{x}, y) \in S}
    y \vec{u}^T \vec{x} \right)
\end{equation}

Possiamo riscrivere il problema precedente in termini del margine $\gamma$:
\begin{equation}
    \begin{aligned}
         & (\gamma^*)^2 = & \max_{\gamma > 0} & \quad \gamma^2                        \\
         &                & \textrm{s.t.}     & \quad \norm{\vec{u}}^2 = 1            \\
         &                &                   & \quad y \vec{u}^T \vec{x} \geq \gamma
        \quad \forall  \left(\vec{x}, y\right) \in S                                  \\
    \end{aligned}
\end{equation}
Notiamo che $\gamma^*$ \`e il margine minimo massimizzato da $\vec{u}^*$ dato che $f(\gamma) = \gamma^2$
\`e monotona crescente per $\gamma > 0$.

Generalizziamo il problema considerando il vettore $\vec{w} = \frac{\vec{u}}{\gamma}$, notando che $\norm{\vec{w}}^2 = \frac{1}{\gamma^2}$.
Dividendo i vincoli per $\gamma$ possiamo riscrivere il problema nella forma:
\begin{equation}
    \begin{aligned}
         & \vec{w}^* = & \argmin_{\vec{w} \in \Real^d} & \quad \norm{\vec{w}}^2             \\
         &             & \textrm{s.t.}                 & \quad \gamma^2\norm{\vec{w}}^2 = 1 \\
         &             &                               & \quad y \vec{w}^T \vec{x} \geq 1
        \quad \forall  \left(\vec{x}, y\right) \in S                                        \\
    \end{aligned}
\end{equation}

Dato che $\gamma$ \`e diventata di fatto una variabile libera, possiamo eliminare il primo vincolo. Effettuando alcune manipolazioni algebriche,
otteniamo quest'ultima forma del problema:
\begin{equation}
    \begin{aligned}
        \vec{w}^* = & \argmin_{\vec{w} \in \Real^d}  \frac{\norm{\vec{w}}^2}{2}                                    \\
                    & \textrm{s.t.} \quad y \vec{w}^T \vec{x} \geq 1  \quad \forall  \left(\vec{x}, y\right) \in S \\
    \end{aligned}
    \label{eq:svm}
\end{equation}

Notiamo che nel problema \ref{eq:svm} il vettore da ottimizzare $\vec{w}$ \`e
un generico vettore in $\Real^d$ senza il vincolo di norma unitaria.

\subsection{Pegasos}\label{sec:pegasos}

Pegasos~\cite{shalev2011pegasos} \`e un algoritmo di discesa del gradiente \emph{online} per SVM.
L'algoritmo prevede due parametri: il numero di epoche (cicli di addestramento) $T$ e
il coefficiente di regolarizzazione $\lambda$.

\begin{algorithm}[h]
    \scriptsize
    \caption{Pegasos}\label{alg:pegasos}
    \begin{algorithmic}[1]
        \Function{Pegasos}{$S = \left\{\left(\vec{x}_1, y_1\right), \ldots, \left(\vec{x}_m, y_m\right)\right\}, T \in \Natural^+, \lambda \in \Real$}
        \State{$\vec{w}_1 \gets \vec{0}$}
        \For{$t = 1, \ldots, T$}
        \State{$\eta_t \gets \frac{1}{\lambda t}$}
        \State{Scegli uniformemente a caso $\left(\vec{x}_{i_t}, y_{i_t}\right)$ da $S$.}
        \State{$\vec{w}_{t+1} \gets \vec{w}_t - \eta_t \nabla \ell_{i_t}(\vec{w})$}
        \EndFor{}
        \Return{$\vec{w}_T$}
        \EndFunction{}
    \end{algorithmic}
\end{algorithm}

Si \`e deciso di considerando solamente l'ultimo classificatore $\vec{w}_T$ invece
che la media degli $\vec{w}_i$ perch\'e sperimentalmente si sono ottenuti
risultati migliori.
\medskip

La funzione di perdita $\ell_t(\vec{w})$ utilizzata nell'algoritmo~\ref{alg:pegasos} \`e:
$$
    \ell_t(\vec{w}) = \frac{\lambda}{2}\norm{\vec{w}}^2 + h_t(\vec{w})
$$

Dove $h_t(\vec{w})$ nel caso considerato \`e la funzione \emph{hinge-loss}:
\begin{equation*}
    \begin{aligned}
        h_t(\vec{w}) & = \max\{0, 1 - y_t \vec{w}^T \vec{x}_t\}       \\
                     & = \left[ 1 - y_t \vec{w}^T \vec{x}_t \right]_+ \\
    \end{aligned}
\end{equation*}

Sapendo che il gradiente della funzione di perdita \`e:
\begin{equation*}
    \begin{aligned}
        \nabla \ell_t(\vec{w}) & = \lambda \vec{w} - \nabla h_t(\vec{w})                           \\
                               & = \lambda \vec{w} - y_t \vec{x}_t \Indicator\{ h_t(\vec{w}) > 0\} \\
    \end{aligned}
\end{equation*}

Il calcolo di $\vec{w}_t$ in Pegasos pu\`o essere scritto in forma pi\`u esplicita:
\begin{equation*}
    \begin{aligned}
        \vec{w}_{t+1} & = \vec{w}_t - \eta_t \nabla \ell_{i_t}(\vec{w}_t)                                                        &                                              \\
                      & = \vec{w}_t - \eta_t (\lambda \vec{w}_t - y_{i_t} \vec{x}_{i_t} \Indicator\{ h_{i_t}(\vec{w}_t) > 0\})   &                                              \\
                      & = \vec{w}_t\left(1 - \frac{1}{t}\right) - \eta_t y_{i_t} \vec{x}_t \Indicator\{ h_{i_t}(\vec{w}_t) > 0\} & \mbox{dato che }\eta_t = \frac{1}{\lambda t} \\
    \end{aligned}
\end{equation*}
