#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
from scipy.interpolate import interp1d

ax = plt.axes()
filename = 'training_error_by_epoch_label_33.csv'
df = pd.read_csv(filename, ',')

x = df['T']
y = df['err']
# f = interp1d(x, y, kind='cubic')
xnew = np.linspace(x[0], x[len(x) - 1])
polyfit = np.poly1d(np.polyfit(x, y, 16))

ax.plot(x, y, 'o', markersize=1, c='xkcd:black')
# ax.plot(xnew, f(xnew), '-')
ax.plot(xnew, polyfit(xnew), linewidth=3, c='xkcd:red', alpha=0.75)

ax.set_ybound(lower=0, upper=0.55)
ax.set_ylabel('training error')
ax.set_xlabel('T')
plt.grid()
plt.tight_layout()
plt.legend(['data', 'polynomial fit (degree 16)'], loc='best')
plt.savefig('training_error_by_epochs.pdf', transparent=True)

plt.show()
