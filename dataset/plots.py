#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns

COL_DESC = {
    'T': 'T',
    'lambda': 'λ'
}


def plot_groups(df, by='T', plot_for='lambda'):
    for name, group in df.groupby(by):
        if len(group) > 1:
            group.plot(x=plot_for, y='err', logx=True, label='Avg error')
            plt.title('{} = {}'.format(COL_DESC[by], name))
            plt.grid()
            plt.gca().set_ybound(lower=0, upper=0.55)
            plt.gca().set_ylabel('error')
            plt.gca().set_xlabel(COL_DESC[plot_for])

            plt.savefig('{}{}.pdf'.format(by, name), transparent=True)
            # plt.show()


def plot_heatmap(df):
    fig = plt.figure()
    ax = plt.axes()
    pivoted = df.pivot('T', 'lambda', 'err')
    # replace 'lambda' with greek letter
    pivoted.columns.name = COL_DESC[pivoted.columns.name]
    sns.heatmap(pivoted, annot=True, fmt='0.0%',
                cmap='YlGnBu', ax=ax, vmin=0, vmax=0.5)
    ax.set_ybound(lower=0, upper=5)

    ax.xaxis.set_tick_params(rotation=45)
    ax.yaxis.set_tick_params(rotation=0)

    plt.tight_layout()
    plt.savefig('heatmap.pdf', transparent=True)
    plt.show()


if len(sys.argv) > 1:
    filename = sys.argv[1]
    df = pd.read_csv(filename, ',')

    print(df.groupby(['lambda', 'T']).describe())
    print(df[['err', 'lambda']].groupby('lambda').describe())

    plot_heatmap(df)
    plot_groups(df, by='T', plot_for='lambda')
    plot_groups(df, by='lambda', plot_for='T')
