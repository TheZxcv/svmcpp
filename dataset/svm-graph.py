#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from sklearn import svm
import scipy
from matplotlib import pyplot as plt
import matplotlib
import sys
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np


def plot_points(ax, points, **kwargs):
    X = [p[0] for p in points]
    Y = [p[1] for p in points]

    ax.scatter(X, Y, **kwargs)


ax = plt.gca()
major_ticks = np.arange(-1, 8, 1)
minor_ticks = np.arange(-1, 8, 0.5)

ax.set_xticks(major_ticks)
ax.set_xticks(minor_ticks, minor=True)
ax.set_yticks(major_ticks)
ax.set_yticks(minor_ticks, minor=True)
ax.grid(which='minor', alpha=0.2)
ax.grid(which='major', alpha=0.5)
ax.set_xlim([0, 7])
ax.set_xlim([0, 7])

# And a corresponding grid
ax.grid(True, which='both')


points_pos = [
    [4, 6],
    [6, 5],
    [4, 5],
    [5, 3],
    [3, 5],
]
plot_points(ax, points_pos, c='r', marker='+', s=200)

points_neg = [
    [1, 3],
    [2, 1],
    [3.201116325155198, 1.9533693833875654],
    [2, 3],
]
plot_points(ax, points_neg, c='b', marker='_', s=200)
plt.axis('equal')

X = points_neg + points_pos
Y = [-1]*len(points_neg) + [1]*len(points_pos)

C = 1.0  # SVM regularization parameter
clf = svm.SVC(kernel='linear',  gamma=0.7, C=C)
clf.fit(X, Y)

xlim = ax.get_xlim()
ylim = ax.get_ylim()

# create grid to evaluate model
xx = np.linspace(xlim[0]-10, xlim[1]+10, 30)
yy = np.linspace(ylim[0]-10, ylim[1]+10, 30)
YY, XX = np.meshgrid(yy, xx)
xy = np.vstack([XX.ravel(), YY.ravel()]).T
Z = clf.decision_function(xy).reshape(XX.shape)

# plot decision boundary and margins
ax.contour(XX, YY, Z, colors=['k', 'green', 'k'], levels=[-1, 0, 1],
           alpha=0.5,
           linestyles=['--', '-', '--'])
# plot support vectors
ax.scatter(clf.support_vectors_[:, 0], clf.support_vectors_[:, 1], s=100,
           linewidth=1, facecolors='none', edgecolors='k')


x = clf.coef_[0][0]
y = clf.coef_[0][1]
ori_x = clf.support_vectors_[:, 0][0] + x
ori_y = clf.support_vectors_[:, 1][0] + y

ax.quiver(ori_x, ori_y, x, y, scale=1, units='xy')
ax.set_xlim([0, 7])
ax.set_ylim([0, 7])
plt.savefig('svm-with-supports.pdf', dpi=300)
plt.show()
