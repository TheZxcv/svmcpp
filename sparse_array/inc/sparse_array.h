#ifndef SPARSE_ARRAY_H
#define SPARSE_ARRAY_H

#include <algorithm>
#include <memory>
#include <vector>

namespace custom {

template <typename T, typename TIndex> struct sparse_array_entry {
  using self = sparse_array_entry<T, TIndex>;
  sparse_array_entry(TIndex i, T val) : index{i}, value{val} {}
  TIndex index;
  T value;
};

template <typename T, typename TIndex> class sparse_array_iter;

template <typename T, typename TSize = std::size_t,
          class TAllocator = std::allocator<sparse_array_entry<T, TSize>>>
struct sparse_array {
public:
  using self = sparse_array<T>;
  using value_type = T;
  using size_type = TSize;
  using entry_type = sparse_array_entry<value_type, size_type>;

  using difference_type = ptrdiff_t;
  using allocator_type = TAllocator;

  using reference = value_type &;
  using const_reference = const value_type &;

  using iterator = entry_type *;
  using const_iterator = const entry_type *;
  using reverse_iterator = std::reverse_iterator<iterator>;
  using const_reverse_iterator = std::reverse_iterator<const_iterator>;

  using const_sparse_iterator = sparse_array_iter<value_type, size_type>;

private:
  value_type m_zero{0};
  size_type m_size;
  size_type m_dense_size;
  std::vector<entry_type> m_values;

public:
  explicit sparse_array(const allocator_type &a = allocator_type());
  sparse_array(const sparse_array &) = default;
  sparse_array(sparse_array &&) noexcept = default;

  sparse_array(size_type sparse_size,
               const allocator_type &a = allocator_type()) noexcept;

  sparse_array(size_type sparse_size,
               std::initializer_list<std::tuple<size_type, value_type>> il,
               const allocator_type &a = allocator_type());

  template <typename FwdIterT>
  sparse_array(size_type sparse_size, FwdIterT first, FwdIterT last,
               const allocator_type &a = allocator_type());

  sparse_array &operator=(const sparse_array &) = default;
  sparse_array &operator=(sparse_array &&) noexcept = default;

  ~sparse_array() = default;

  iterator begin();
  const_iterator begin() const;
  const_iterator cbegin() const;
  const_sparse_iterator sparse_cbegin() const;

  iterator end();
  const_iterator end() const;
  const_iterator cend() const;
  const_sparse_iterator sparse_cend() const;

  reverse_iterator rbegin();
  const_reverse_iterator rbegin() const;
  const_reverse_iterator crbegin() const;

  reverse_iterator rend();
  const_reverse_iterator rend() const;
  const_reverse_iterator crend() const;

  bool empty() const;
  size_type size() const;
  size_type max_size() const;

  entry_type *data();
  const entry_type *data() const;

  const_reference operator[](size_type i) const;
  const_reference at(size_type i) const;
  const_reference front() const;
  const_reference back() const;
};

template <typename T, typename TSize, class TAllocator>
inline sparse_array<T, TSize, TAllocator>::sparse_array(const allocator_type &a)
    : m_size{0}, m_dense_size{0}, m_values{a} {}

template <typename T, typename TSize, class TAllocator>
inline sparse_array<T, TSize, TAllocator>::sparse_array(
    size_type sparse_size, const allocator_type &allocator) noexcept
    : m_size{sparse_size}, m_dense_size{0}, m_values{allocator} {}

template <typename T, typename TSize, class TAllocator>
inline sparse_array<T, TSize, TAllocator>::sparse_array(
    size_type sparse_size,
    std::initializer_list<std::tuple<size_type, value_type>> ilist,
    const allocator_type &allocator)
    : m_size{sparse_size}, m_dense_size{ilist.size()}, m_values{allocator} {

  m_values.reserve(m_dense_size);
  for (auto val : ilist) {
    m_values.emplace_back(std::get<0>(val), std::get<1>(val));
  }

  std::sort(std::begin(m_values), std::end(m_values),
            [](const auto &a, const auto &b) { return a.index < b.index; });

  for (size_type i = 0; i < m_values.size() - 1; i++) {
    if (m_values[i].index == m_values[i + 1].index) {
      std::exit(-1);
    }
  }
}

template <typename T, typename TSize, class TAllocator>
template <typename FwdIterT>
inline sparse_array<T, TSize, TAllocator>::sparse_array(
    size_type sparse_size, FwdIterT first, FwdIterT last,
    const allocator_type &allocator)
    : m_size{sparse_size}, m_values{allocator} {

  static_assert(
      std::is_same<
          typename std::iterator_traits<FwdIterT>::value_type,
          std::tuple<typename sparse_array<T, TSize, TAllocator>::size_type,
                     typename sparse_array<T, TSize, TAllocator>::value_type>>::
          value,
      "The value_type must be specific_value_type.");

  for (auto it = first; it != last; ++it) {
    m_values.emplace_back(std::get<0>(*it), std::get<1>(*it));
  }

  m_dense_size = m_values.size();

  std::sort(std::begin(m_values), std::end(m_values),
            [](const auto &a, const auto &b) { return a.index < b.index; });

  for (size_type i = 0; i < m_values.size() - 1; i++) {
    if (m_values[i].index == m_values[i + 1].index) {
      std::exit(-1);
    }
  }
}

template <typename T, typename TSize, class TAllocator>
inline typename sparse_array<T, TSize, TAllocator>::iterator
sparse_array<T, TSize, TAllocator>::begin() {
  return m_values.data();
}

template <typename T, typename TSize, class TAllocator>
inline typename sparse_array<T, TSize, TAllocator>::const_iterator
sparse_array<T, TSize, TAllocator>::begin() const {
  return m_values.data();
}

template <typename T, typename TSize, class TAllocator>
inline typename sparse_array<T, TSize, TAllocator>::const_iterator
sparse_array<T, TSize, TAllocator>::cbegin() const {
  return m_values.data();
}

template <typename T, typename TSize, class TAllocator>
inline typename sparse_array<T, TSize, TAllocator>::const_sparse_iterator
sparse_array<T, TSize, TAllocator>::sparse_cbegin() const {
  return {cbegin(), cend(), m_size, 0, 0};
}

template <typename T, typename TSize, class TAllocator>
inline typename sparse_array<T, TSize, TAllocator>::iterator
sparse_array<T, TSize, TAllocator>::end() {
  return m_values.data() + m_dense_size;
}

template <typename T, typename TSize, class TAllocator>
inline typename sparse_array<T, TSize, TAllocator>::const_iterator
sparse_array<T, TSize, TAllocator>::end() const {
  return m_values.data() + m_dense_size;
}

template <typename T, typename TSize, class TAllocator>
inline typename sparse_array<T, TSize, TAllocator>::const_iterator
sparse_array<T, TSize, TAllocator>::cend() const {
  return m_values.data() + m_dense_size;
}

template <typename T, typename TSize, class TAllocator>
inline typename sparse_array<T, TSize, TAllocator>::const_sparse_iterator
sparse_array<T, TSize, TAllocator>::sparse_cend() const {
  return {cbegin(), cend(), m_size, m_dense_size, m_size};
}

template <typename T, typename TSize, class TAllocator>
inline typename sparse_array<T, TSize, TAllocator>::reverse_iterator
sparse_array<T, TSize, TAllocator>::rbegin() {
  return reverse_iterator(m_values.data() + m_dense_size);
}

template <typename T, typename TSize, class TAllocator>
inline typename sparse_array<T, TSize, TAllocator>::const_reverse_iterator
sparse_array<T, TSize, TAllocator>::rbegin() const {
  return const_reverse_iterator(m_values.data() + m_dense_size);
}

template <typename T, typename TSize, class TAllocator>
inline typename sparse_array<T, TSize, TAllocator>::const_reverse_iterator
sparse_array<T, TSize, TAllocator>::crbegin() const {
  return const_reverse_iterator(m_values.data() + m_dense_size);
}

template <typename T, typename TSize, class TAllocator>
inline typename sparse_array<T, TSize, TAllocator>::reverse_iterator
sparse_array<T, TSize, TAllocator>::rend() {
  return reverse_iterator(m_values.data());
}

template <typename T, typename TSize, class TAllocator>
inline typename sparse_array<T, TSize, TAllocator>::const_reverse_iterator
sparse_array<T, TSize, TAllocator>::rend() const {
  return const_reverse_iterator(static_cast<const_iterator>(m_values.data()));
}

template <typename T, typename TSize, class TAllocator>
inline typename sparse_array<T, TSize, TAllocator>::const_reverse_iterator
sparse_array<T, TSize, TAllocator>::crend() const {
  return const_reverse_iterator(static_cast<const_iterator>(m_values.data()));
}

template <typename T, typename TSize, class TAllocator>
inline typename sparse_array<T, TSize, TAllocator>::size_type
sparse_array<T, TSize, TAllocator>::size() const {
  return m_size;
}

template <typename T, typename TSize, class TAllocator>
inline typename sparse_array<T, TSize, TAllocator>::size_type
sparse_array<T, TSize, TAllocator>::max_size() const {
  return m_size;
}

template <typename T, typename TSize, class TAllocator>
inline bool sparse_array<T, TSize, TAllocator>::empty() const {
  return (m_size == 0);
}

template <typename T, typename TSize, class TAllocator>
inline typename sparse_array<T, TSize, TAllocator>::const_reference
    sparse_array<T, TSize, TAllocator>::operator[](size_type i) const {
  auto first = std::lower_bound(
      m_values.begin(), m_values.end(), i,
      [](const auto &a, const sparse_array<T, TSize, TAllocator>::size_type b) {
        return a.index < b;
      });
  return first != m_values.end() && first->index == i ? first->value : m_zero;
}

template <typename T, typename TSize, class TAllocator>
inline typename sparse_array<T, TSize, TAllocator>::const_reference
sparse_array<T, TSize, TAllocator>::front() const {
  return m_values[0].index == 0 ? m_values[0].value : m_zero;
}

template <typename T, typename TSize, class TAllocator>
inline typename sparse_array<T, TSize, TAllocator>::const_reference
sparse_array<T, TSize, TAllocator>::back() const {
  return m_values[m_dense_size - 1].index == m_size - 1
             ? m_values[m_dense_size - 1].value
             : m_zero;
}

template <typename T, typename TSize, class TAllocator>
inline typename sparse_array<T, TSize, TAllocator>::entry_type *
sparse_array<T, TSize, TAllocator>::data() {
  return m_values.data();
}

template <typename T, typename TSize, class TAllocator>
inline const typename sparse_array<T, TSize, TAllocator>::entry_type *
sparse_array<T, TSize, TAllocator>::data() const {
  return m_values.data();
}

template <typename T, typename TSize, class TAllocator>
inline typename sparse_array<T, TSize, TAllocator>::const_reference
sparse_array<T, TSize, TAllocator>::at(size_type i) const {
  auto first = std::lower_bound(
      m_values.begin(), m_values.end(), i,
      [](const auto &a, const sparse_array<T, TSize, TAllocator>::size_type b) {
        return a.index < b;
      });
  return first != m_values.end() && first->index == i ? first->value : m_zero;
}

///////////////////////////////////////////////////////////////////////
// global operators
///////////////////////////////////////////////////////////////////////

template <typename T, typename TSize, class TAllocator>
inline bool operator==(const sparse_array<T, TSize, TAllocator> &a,
                       const sparse_array<T, TSize, TAllocator> &b) {
  if (a.m_size != a.m_size)
    return false;
  if (a.m_dense_size != a.m_dense_size)
    return false;
  return std::equal(&a.m_values[0], &a.m_values[a.m_dense_size],
                    &b.m_values[0]);
}

template <typename T, typename TSize, class TAllocator>
inline bool operator!=(const sparse_array<T, TSize, TAllocator> &a,
                       const sparse_array<T, TSize, TAllocator> &b) {
  if (a.m_size == a.m_size)
    return false;
  if (a.m_dense_size == a.m_dense_size)
    return false;
  return !std::equal(&a.m_values[0], &a.m_values[a.m_dense_size],
                     &b.m_values[0]);
}

template <typename T, typename TIndex>
class sparse_array_iter : public std::iterator<std::forward_iterator_tag, T> {
public:
  using self = sparse_array_iter<T, TIndex>;
  using self_reference = sparse_array_iter<T, TIndex> &;

  sparse_array_iter(const sparse_array_entry<T, TIndex> *first,
                    const sparse_array_entry<T, TIndex> *last, TIndex length,
                    TIndex start_dense = 0, TIndex start_index = 0)
      : m_length{length}, m_first{first}, m_last{last},
        m_curr_dense_index{start_dense}, m_curr_index{start_index} {}

  sparse_array_iter(const self &other) noexcept = default;
  sparse_array_iter(self &&) noexcept = default;

  self_reference &operator=(const self &) = default;
  self_reference &operator=(self &&) noexcept = default;

  ~sparse_array_iter() = default;

  T operator*() const {
    if (m_first + m_curr_dense_index != m_last &&
        (m_first + m_curr_dense_index)->index == m_curr_index) {
      return (m_first + m_curr_dense_index)->value;
    }
    return 0;
  }

  self &operator++(int) {
    if (m_curr_index < m_length) {
      m_curr_index++;
      if (m_first + m_curr_dense_index != m_last &&
          (m_first + m_curr_dense_index)->index < m_curr_index) {
        m_curr_dense_index++;
      }
    }
    return *this;
  }

  bool operator==(const self &other) {
    return m_length == other.m_length && m_first == other.m_first &&
           m_last == other.m_last &&
           m_curr_dense_index == other.m_curr_dense_index &&
           m_curr_index == other.m_curr_index;
  }

  bool operator!=(const self &other) { return !((*this) == other); }

private:
  TIndex m_length;
  const sparse_array_entry<T, TIndex> *m_first;
  const sparse_array_entry<T, TIndex> *m_last;
  TIndex m_curr_dense_index;
  TIndex m_curr_index;
};

} // namespace custom

#endif
