#include "sparse_array.h"

#include <gtest/gtest.h>
#include <sstream>

using namespace custom;

namespace {

TEST(sparse_array_tests, empty) {
  sparse_array<int> sparse{};
  ASSERT_EQ(0, sparse.size());
}

TEST(sparse_array_tests, no_entries) {
  sparse_array<int> sparse{4};

  EXPECT_EQ(0, sparse[0]);
  EXPECT_EQ(0, sparse[1]);
  EXPECT_EQ(0, sparse[2]);
  EXPECT_EQ(0, sparse[3]);
}

TEST(sparse_array_tests, one_entry) {
  sparse_array<int> sparse{4, {{1, 2}}};

  EXPECT_EQ(0, sparse[0]);
  EXPECT_EQ(2, sparse[1]);
  EXPECT_EQ(0, sparse[2]);
  EXPECT_EQ(0, sparse[3]);
}

TEST(sparse_array_tests, ctor_with_iterator) {
  std::vector<std::tuple<size_t, float>> entries{{0, 0.25}, {2, 0.125}};
  sparse_array<float> sparse{4, std::begin(entries), std::end(entries)};

  EXPECT_FLOAT_EQ(0.25, sparse[0]);
  EXPECT_FLOAT_EQ(0, sparse[1]);
  EXPECT_FLOAT_EQ(0.125, sparse[2]);
  EXPECT_FLOAT_EQ(0, sparse[3]);
}

} // namespace
