#ifndef RANDOM_GENERATOR_H
#define RANDOM_GENERATOR_H

#include <random>

namespace PRNG_holder {
extern std::mt19937 generator;
}

#endif
