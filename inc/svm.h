#ifndef SVM_H
#define SVM_H

#include "dataset.h"
#include "random_generator.h"

#include <array>
#include <functional>
#include <iomanip>
#include <iostream>
#include <random>

namespace svm {

using dataset::document;

constexpr float DEFAULT_LAMBDA = 0.5;
constexpr int DEFAULT_EPOCHS = 1000;

template <size_t N>
inline float scalar_prod(const std::array<float, N> &w,
                         const custom::sparse_array<float> &x) {
  float res = 0.0;
  for (auto pair : x) {
    res += w[pair.index] * pair.value;
  }
  return res;
}

template <size_t N>
inline float loss(const std::array<float, N> &w, int y,
                  const custom::sparse_array<float> &x) {
  return 1.0f - y * scalar_prod(w, x);
}

template <size_t N>
double calculate_error(const std::array<float, N> &w,
                       const std::vector<const document *> &set,
                       unsigned int MASK) {
  int nerrors = 0;
  for (auto doc : set) {
    int y = (doc->labels & MASK) ? 1 : -1;
    int y_hat = scalar_prod(w, doc->features) >= 0 ? 1 : -1;
    if (y_hat != y) {
      nerrors++;
    }
  }
  return static_cast<double>(nerrors) / set.size();
}

/**
 * Updates w in-place.
 */
template <size_t N>
inline void update_gradient_loss(std::array<float, N> &w, int y,
                                 const custom::sparse_array<float> &x,
                                 float eta, float lambda) {
  // if (y * scalar_prod(w, x) < 1) {
  if (loss(w, y, x) > 0) {
    auto it = std::begin(x);
    auto end = std::end(x);

    for (size_t i = 0; i < N; i++) {
      float val = 0;
      if (it != end && it->index == i) {
        val = it->value;
        ++it;
      }

      w[i] = (1 - eta * lambda) * w[i] + eta * y * val;
    }
  } else {
    for (size_t i = 0; i < N; i++) {
      w[i] = (1 - eta * lambda) * w[i];
    }
  }
}

template <size_t N>
std::array<float, N> pegasos(const std::vector<const document *> &training_set,
                             const std::function<int(const document &)> &f,
                             int epochs = DEFAULT_EPOCHS,
                             float lambda = DEFAULT_LAMBDA) {
  std::uniform_int_distribution<size_t> distribution(0,
                                                     training_set.size() - 1);

  std::array<float, N> w{};
  w.fill(0.0);
  for (int t = 1; t <= epochs; t++) {
    float eta = 1.0f / (t * lambda);
    size_t index = distribution(PRNG_holder::generator);
    auto &doc = training_set[index];
    update_gradient_loss(w, f(*doc), doc->features, eta, lambda);
  }
  return std::move(w);
}

/**
 * K-fold validation.
 */
template <size_t N>
float kfold_validation(const std::vector<const document *> &training_set,
                       const unsigned int label_mask, int T, float lambda) {
  using doc_vec = std::vector<const document *>;

  constexpr int FOLDS = 5;
  const doc_vec::difference_type fold_size = training_set.size() / FOLDS;

  std::vector<const document *> val_training_set{};
  std::vector<const document *> val_test_set{};

  float avg = 0;
  for (unsigned fold = 0; fold < FOLDS; fold++) {
    auto fold_begin = std::next(std::begin(training_set), fold * fold_size);
    // if it's past the end, clamp it (it should never be necessary given how
    // fold_size is calculated)
    auto fold_end = (std::end(training_set) - fold_begin >= fold_size)
                        ? std::next(fold_begin, fold_size)
                        : std::end(training_set);

    val_test_set.resize(static_cast<doc_vec::size_type>(fold_end - fold_begin));
    val_training_set.resize(training_set.size() - val_test_set.size());

    auto rest = std::copy(std::begin(training_set), fold_begin,
                          std::begin(val_training_set));
    std::copy(fold_end, std::end(training_set), rest);
    std::copy(fold_begin, fold_end, std::begin(val_test_set));

    auto w = pegasos<N>(
        val_training_set,
        [label_mask](auto &doc) { return (doc.labels & label_mask) ? 1 : -1; },
        T, lambda);

    avg += calculate_error(w, val_test_set, label_mask);
  }

  return avg / static_cast<float>(FOLDS);
}

/**
 * Estimate best parameter values by Exhaustive Grid Search
 */
template <size_t N>
std::tuple<int, float> estimate_parameters(
    const std::vector<const document *> &training_set,
    const unsigned int label_mask, const std::vector<int> &T_values,
    const std::vector<float> &lambda_values,
    const std::function<void(int, float, float)> record_performance = nullptr) {
  float best_error = std::numeric_limits<float>::max();
  float best_lambda = -1;
  int best_T = -1;
  for (auto T : T_values) {
    for (auto lambda : lambda_values) {
      float estimate_error =
          kfold_validation<N>(training_set, label_mask, T, lambda);

      if (record_performance != nullptr) {
        record_performance(T, lambda, estimate_error);
      }

      if (estimate_error < best_error) {
        best_T = T;
        best_lambda = lambda;
        best_error = estimate_error;
      }
    }
  }

  return std::make_tuple(best_T, best_lambda);
}

template <size_t N>
void write_classifier_to_file(std::ostream &ostream,
                              const std::array<float, N> &w) {
  for (const auto val : w) {
    ostream << val << '\n';
  }
}
} // namespace svm

#endif
