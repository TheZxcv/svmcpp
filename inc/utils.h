#ifndef UTILS_H
#define UTILS_H

#include <sstream>
#include <string>

[[noreturn]] void error(const std::string &message);
#ifdef NDEBUG
#define debug_assert(_cond, _message) ((void)0)
#else
void __debug_assert(bool condition, const std::string &message);
#define debug_assert(condition, message) __debug_assert(condition, message)
#endif

#endif
