#ifndef CLI_H
#define CLI_H

#include "dataset.h"
#include "svm.h"

#include <string>

namespace cli {

struct arg_options {
  std::string dataset;
  bool tune_parameters = true;
  float lambda = svm::DEFAULT_LAMBDA;
  int epochs = svm::DEFAULT_EPOCHS;
  std::vector<unsigned int> labels{};
  std::string input_parameters_file{};
  std::vector<int> T_values{};
  std::vector<float> lambda_values{};
};

struct arg_options parse_args(int argc, char *argv[]);

} // namespace cli

#endif
