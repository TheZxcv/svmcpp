#ifndef DATASET_H
#define DATASET_H

#include "sparse_array.h"

#include <istream>
#include <vector>

namespace dataset {

struct document {
  unsigned int labels;
  custom::sparse_array<float> features;
};

constexpr unsigned int MOST_COMMON_LABELS[] = {33, 70, 101, 4};

// MAX_DENSE_COORDINATE_SEEN + 1
constexpr size_t FEATURE_SPACE = (95280 + 1);

std::vector<document> read(std::istream &input_stream);

} // namespace dataset

#endif
