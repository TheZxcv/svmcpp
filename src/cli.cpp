#include "cli.h"

#include "utils.h"

#include <fstream>
#include <getopt.h>
#include <iostream>
#include <istream>
#include <string>

namespace cli {

static void print_usage(const std::string &program) {
  struct arg_options defaults {};
  std::cout << "Usage: " << program
            << " [-nh] [-e epochs] [-l lambda] [DATASET FILE]" << std::endl
            << "Options and arguments:" << std::endl
            << " -n        disable parameters tuning (default: "
            << !defaults.tune_parameters << ")" << std::endl
            << " -e EPOCHS training epochs (default: " << defaults.epochs << ")"
            << std::endl
            << " -l LAMBDA value for the parameter λ (default: "
            << defaults.lambda << ")" << std::endl
            << " -y LABEL  trains the classifier only on the given labels "
               "(default: all labels: "
               "0, 1, 2, 3)."
            << std::endl
            << " -p FILE   file containing all possible values for T and λ. "
            << "Used for best parameters estimation." << std::endl
            << " -h        prints this usage message" << std::endl;
}

static bool read_params_file(struct arg_options &options,
                             const std::string &input_parameters_file) {
  std::ifstream params{input_parameters_file, std::ifstream::in};
  if (params.fail()) {
    std::cerr << "Failed to open file." << std::endl;
    return false;
  }

  std::string line;
  while (std::getline(params, line)) {
    std::istringstream iss{line};

    char type;
    iss >> type;

    if (type == 'T') {
      options.T_values.clear();
      int val;
      while (iss >> val) {
        if (val <= 0) {
          std::cerr << "Invalid value for epochs: " << val << "." << std::endl;
          return false;
        }
        options.T_values.push_back(val);
      }
    } else if (type == 'l') {
      options.lambda_values.clear();
      float val;
      while (iss >> val) {
        if (val <= 0 || !std::isfinite(options.lambda)) {
          std::cerr << "Invalid value for lambda: " << val << "." << std::endl;
          return false;
        }
        options.lambda_values.push_back(val);
      }
    }
  }

  return !(options.T_values.empty() || options.lambda_values.empty());
}

struct arg_options parse_args(int argc, char *argv[]) {
  if (argc <= 1) {
    error("not enough arguments.");
  }

  bool default_labels = true;
  bool default_params = true;
  constexpr size_t LABELS_LENGTH = sizeof(dataset::MOST_COMMON_LABELS) /
                                   sizeof(*dataset::MOST_COMMON_LABELS);
  struct arg_options options {};

  int opt;
  opterr = 0;
  while ((opt = getopt(argc, argv, "l:e:ny:p:h")) != EOF) {
    switch (opt) {
    case 'l': {
      try {
        options.lambda = std::stof(optarg);
      } catch (std::invalid_argument &) {
        std::cerr << "Invalid lambda `" << optarg << "` provided." << std::endl;
        std::exit(-1);
      }

      if (!std::isfinite(options.lambda)) {
        std::cerr << "Invalid value for lambda: " << options.lambda << "."
                  << std::endl;
        std::exit(-1);
      }
    } break;
    case 'e': {
      try {
        options.epochs = std::stoi(optarg);
      } catch (std::invalid_argument &) {
        std::cerr << "Invalid epochs `" << optarg << "` provided." << std::endl;
        std::exit(-1);
      }

      if (options.epochs <= 0) {
        std::cerr << "Invalid value for epochs: " << options.epochs << "."
                  << std::endl;
        std::exit(-1);
      }
    } break;
    case 'n':
      options.tune_parameters = false;
      break;
    case 'y': {
      default_labels = false;

      int label_idx = std::stoi(optarg);
      if (label_idx < 0 || static_cast<unsigned>(label_idx) >= LABELS_LENGTH) {
        std::cerr << "Invalid label `" << label_idx << "` provided."
                  << std::endl;
        std::exit(-1);
      }
      options.labels.push_back(static_cast<unsigned>(label_idx));
    } break;

    case 'p': {
      default_params = false;

      options.input_parameters_file = optarg;
    } break;

    case 'h':
      print_usage(argv[0]);
      std::exit(0);
    case '?':
      if (optopt == 'l' || optopt == 'e')
        std::cerr << "Option -" << static_cast<char>(optopt)
                  << " requires a value" << std::endl;
      else
        std::cerr << "Unknown option `-" << static_cast<char>(optopt) << "'."
                  << std::endl;
      print_usage(argv[0]);
      std::exit(-1);
    default:
      std::abort();
    }
  }

  if (default_labels) {
    options.labels.resize(LABELS_LENGTH);
    std::iota(options.labels.begin(), options.labels.end(), 0);
  }

  if (default_params) {
    options.T_values = {10, 100, 1000, 2000, 5000};
    options.lambda_values = {0.0001f, 0.001f, 0.01f, 0.1f, 1, 10, 100, 1000};
  } else {
    if (!read_params_file(options, options.input_parameters_file)) {
      std::cerr << "Invalid parameters file `" << options.input_parameters_file
                << "` provided." << std::endl;
      std::exit(-1);
    }
  }

  if (argv[optind] == nullptr) {
    std::cerr << "No dataset provided." << std::endl;
    std::exit(0);
  }
  options.dataset = argv[optind];

  return options;
}
} // namespace cli
