#include "cli.h"
#include "random_generator.h"
#include "svm.h"
#include "utils.h"

#include <array>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <istream>
#include <random>
#include <sstream>

using dataset::document;

/**
 * Take `n` elements from src1 and `n` elements from src2 and place them into
 * `dst`.
 */
template <typename T>
void take_n_from_two(std::vector<T> &dst, const std::vector<T> &src1,
                     const std::vector<T> &src2,
                     typename std::vector<T>::size_type n,
                     typename std::vector<T>::difference_type offset = 0) {
  dst.resize(2 * n);

  auto half_size = static_cast<typename std::vector<T>::difference_type>(n);
  auto begin_src1 = std::next(std::begin(src1), offset);
  auto rest =
      std::copy(begin_src1, std::next(begin_src1, half_size), std::begin(dst));

  auto begin_src2 = std::next(std::begin(src2), offset);
  std::copy(begin_src2, std::next(begin_src2, half_size), rest);
}

template <size_t N>
void print_error(const std::array<float, N> &w,
                 const std::vector<const document *> &set, unsigned int MASK) {
  auto error = svm::calculate_error(w, set, MASK);
  std::cout << "err%: " << std::setprecision(2) << std::fixed << error;
  std::cout << " - hit%: " << std::setprecision(2) << std::fixed << 1.0 - error
            << std::endl;
}

class accuracy_printer {
private:
  std::ostream &output;

public:
  explicit accuracy_printer(std::ostream &os) : output{os} {}

  void print_header() { output << "T,lambda,acc,err\n"; }

  void operator()(int T, float lambda, float avg_error) {
    std::cout << "[T:" << std::setw(4) << T << ", λ: " << std::setw(9)
              << std::fixed << std::setprecision(4) << lambda
              << "] -> avg error: " << std::setprecision(2) << std::fixed
              << avg_error << std::endl;

    output << T << ", " << lambda << ", " << 1 - avg_error << ", " << avg_error
           << '\n';
  }
};

std::array<float, dataset::FEATURE_SPACE>
train_on_label(const struct cli::arg_options &options,
               const std::vector<document> &docs, unsigned int label_idx) {
  const unsigned int label_mask = 1U << label_idx;

  // Split positive and negative examples
  std::vector<const document *> positives{};
  std::vector<const document *> negatives{};
  for (const auto &doc : docs) {
    if (doc.labels & label_mask) {
      positives.push_back(&doc);
    } else {
      negatives.push_back(&doc);
    }
  }

  std::shuffle(std::begin(positives), std::end(positives),
               PRNG_holder::generator);
  std::shuffle(std::begin(negatives), std::end(negatives),
               PRNG_holder::generator);

  // Create training set (~75% of the dataset)
  // (half positive and half negative examples)
  constexpr float size_factor = 0.75;
  std::vector<const document *> training_set{};
  {
    const size_t half_training_set_size = static_cast<size_t>(
        size_factor * std::min(positives.size(), negatives.size()));
    std::cout << "train size: " << std::setw(5) << 2 * half_training_set_size
              << std::endl;

    take_n_from_two(training_set, positives, negatives, half_training_set_size);
    std::shuffle(std::begin(training_set), std::end(training_set),
                 PRNG_holder::generator);
  }

  // Create test set (~25% of the dataset)
  // (half positive and half negative examples)
  std::vector<const document *> test_set{};
  {
    const size_t half_test_set_size =
        std::min(positives.size(), negatives.size()) - training_set.size() / 2;
    std::cout << "test size:  " << std::setw(5) << 2 * half_test_set_size
              << std::endl;

    take_n_from_two(test_set, positives, negatives, half_test_set_size,
                    training_set.size() / 2);
  }

  int T = options.epochs;
  float lambda = options.lambda;

  if (options.tune_parameters) {
    std::ostringstream oss{};
    oss << "validation_label_" << dataset::MOST_COMMON_LABELS[label_idx]
        << ".csv";
    std::ofstream csv_val{oss.str()};
    accuracy_printer printer{csv_val};

    printer.print_header();
    auto parameters = svm::estimate_parameters<dataset::FEATURE_SPACE>(
        training_set, label_mask, options.T_values, options.lambda_values,
        printer);
    T = std::get<0>(parameters);
    lambda = std::get<1>(parameters);

    csv_val.flush();
    csv_val.close();
  }
  std::cout << "epochs: " << T << std::endl;
  std::cout << "λ:      " << std::setprecision(4) << lambda << std::endl;

  auto w = svm::pegasos<dataset::FEATURE_SPACE>(
      training_set,
      [label_mask](auto &doc) { return (doc.labels & label_mask) ? 1 : -1; }, T,
      lambda);

  std::cout << "Training error: " << std::endl;
  print_error(w, training_set, label_mask);
  std::cout << "Test error: " << std::endl;
  print_error(w, test_set, label_mask);
  std::cout << std::endl;

  return w;
}

int main(int argc, char *argv[]) {
  PRNG_holder::generator.seed(0);
  auto options = cli::parse_args(argc, argv);

  std::ifstream ifs{options.dataset, std::ifstream::in};
  if (ifs.fail()) {
    error("failed to open file.");
  }

  auto docs = dataset::read(ifs);
  for (auto label_idx : options.labels) {
    std::cout << "Label[" << label_idx
              << "]: " << dataset::MOST_COMMON_LABELS[label_idx] << std::endl;
    auto w = train_on_label(options, docs, label_idx);

    std::ostringstream oss{};
    oss << "classifier_" << dataset::MOST_COMMON_LABELS[label_idx];
    std::ofstream output{oss.str()};
    svm::write_classifier_to_file(output, w);
  }
  return 0;
}
