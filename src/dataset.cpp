#include "dataset.h"

#include <sstream>

namespace dataset {

std::vector<document> read(std::istream &input_stream) {
  std::string line{};
  std::vector<document> docs;

  std::vector<std::tuple<size_t, float>> features;
  while (std::getline(input_stream, line)) {

    std::istringstream iss{line};
    std::string doc_id;
    iss >> doc_id;

    unsigned int labels = 0;
    unsigned int label;
    while (iss >> label) {
      for (unsigned int i = 0; i < 4; i++) {
        if (label == MOST_COMMON_LABELS[i]) {
          labels |= 1U << i;
        }
      }
    }
    iss.clear();

    iss.ignore(std::numeric_limits<std::streamsize>::max(), '=');

    size_t index;
    float value;
    while (iss >> index) {
      iss.ignore(std::numeric_limits<std::streamsize>::max(), ':');
      iss >> value;
      features.emplace_back(index, value);
    }
    // no bias

    document doc{labels,
                 {FEATURE_SPACE, std::begin(features), std::end(features)}};
    features.clear();

    docs.emplace_back(std::move(doc));
  }

  return docs;
}

} // namespace dataset
