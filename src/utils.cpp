#include "utils.h"

#include <iostream>

#define ANSI_RED "\x1b[31m"
#define ANSI_RESET "\x1b[0m"

/**
 * Prints an error message and terminate the execution.
 * @param message the error message
 **/
[[noreturn]] void error(const std::string &message) {
  std::cerr << ANSI_RED "Error" ANSI_RESET ": " << message << std::endl;
  std::exit(-1);
}

void __debug_assert(bool condition, const std::string &message) {
  if (!condition)
    error(message);
}
