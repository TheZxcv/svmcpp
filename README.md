# SVMcpp

## News categorization with Pegasos

Download [this](https://www.dropbox.com/s/grnat79d2n0q73n/SingerSplit_sorted_20k.data.zip?dl=0) portion of the Reuters dataset
containing 20K newsitems. The dataset is multiclass and multilabel (more that
one label for each data point). Every line of the file has the following
format:

docId labList = feature:val feature:val feature:val ...

where docId is a unique identifier for the newsitem (drop it before training),
labList is a space-separated list of numerical labels, and feature:val are
feature-value pairs. Each feature is associated with a word and the value is a
function of the frequency of the word in the newsitem. The coding is sparse:
only the features with non-zero value are listed. Implement Pegasos from
scratch and use it to train four binary classifiers (one-vs-all encoding), each
recognizing the presence of one of the four most frequent labels in the dataset
(the other labels can be ignored). Study the classification performance for
different values of the parameters λ and T in Pegasos. Use external
cross-validation to evaluate accuracy.

Extra (for groups only): Implement the Perceptron from scratch, and compare
against Pegasos its classification performance on the same dataset (one-vs-all
encoding as before) considering the predictor obtained by averaging all the
Perceptron models. Consider different numbers of epochs on the training set. 

## Top 4 Most Frequent Labels

| Label | Frequency | Occurrences |
|------:|----------:|------------:|
|   33  |   44.45%  |     8899    |
|   70  |   28.17%  |     5634    |
|  101  |   24.37%  |     4874    |
|    4  |   17.32%  |     3465    |

 - Features Space size: 95281 dimensions
 - Avg. density: 94 / 95281 (~0.099%)
 - Min density: 9 / 95281 (~0.0095%)
 - Max density: 1363 / 95281 (~1.43%)

## Build

```bash
$ mkdir build
$ cd build
$ cmake -DCMAKE_BUILD_TYPE=Release ..
$ make
```
## Usage

```bash
$ svmcpp-bin -h
Usage: ./svmcpp-bin [-nh] [-e epochs] [-l lambda] [DATASET FILE]
Options and arguments:
 -n        disable parameters tuning (default: 0)
 -e EPOCHS training epochs (default: 1000)
 -l LAMBDA value for the parameter λ (default: 0.5)
 -y LABEL  trains the classifier only on the given labels (default: all labels: 0, 1, 2, 3).
 -p FILE   file containing all possible values for T and λ. Used for best parameters estimation.
 -h        prints this usage message
```
